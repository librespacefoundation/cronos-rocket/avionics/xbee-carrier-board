# Xbee Carrier Board

A PocketQube format carrier board for [XBee-PRO 900HP S3B RPSMA Antenna](https://www.hellasdigital.gr/go-create/arduino-shields-and-accessories/xbee-pro-900hp-s3b-rpsma-antenna-tel0021/).

## Libraries :
Besides a project specific library that is included in this repo,
one should download LSF Kicad library (https://gitlab.com/librespacefoundation/lsf-kicad-lib.git)
and set LSF_KICAD_LIB as an environment variable in their system.